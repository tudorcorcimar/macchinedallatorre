<?php

use App\Models\Services\Captcha;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {

    Route::group(array('namespace' => '\Frontend'), function() {

        Route::group(array('namespace' => '\Home'), function() {
            Route::get('/', ['uses' => 'Home@showHome']);
            Route::get('/aboutus', ['uses' => 'Home@showAboutUs']);
            Route::get('/contacts', ['uses' => 'Contacts@showContacts']);
            Route::post('/contacts', ['uses' => 'Contacts@send']);
        });

        Route::group(array('namespace' => '\Macchine'), function() {
            Route::get('/macchine', ['uses' => 'Macchine@showMacchine']);
        });

    });

    Route::group(array('namespace' => '\Admin'), function() {

        Route::group(array('namespace' => '\NonDbAuth'), function() {
            Route::get('/admin', ['uses' => 'Auth@showSignIn']);
            Route::get('/admin/login', ['uses' => 'Auth@showSignIn']);
            Route::post('/admin/login', ['uses' => 'Auth@SignIn']);
        });

        Route::group(array('namespace' => '\Macchine'), function() {
            Route::get('/admin/macchine', ['uses' => 'Macchine@showMacchine']);
            Route::post('/admin/macchine/add', ['uses' => 'Macchine@addMacchine']);
            Route::get('/admin/macchine/{id}/remove', ['uses' => 'Macchine@removeMacchine']);
        });

    });

    /**
     * Get captcha
     */
    Route::get('/captcha', function(){
        return Captcha::getCaptcha();
    });
    
});
