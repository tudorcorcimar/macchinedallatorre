<?php

namespace App\Http\Controllers\Admin\Macchine;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class Macchine extends BaseController
{
    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function showMacchine(){
        if(!$this->isAuth){
            return Redirect::to('admin/login');
        }
        return View::make("admin.macchine", $this->viewBag);
    }
    
    public function addMacchine() {
        if(!$this->isAuth){
            return Redirect::to('admin/login');
        }
        $input = Input::all();
        $this->viewBag['input'] = $input;
        $images = Input::file('photo');
        $rules = array(
            'name'  => 'required',
            'description'  => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $this->viewBag['error'] = $validator->errors()->first();
            return View::make("admin.macchine", $this->viewBag);
        }
        $macchinePath = public_path('resources/macchine/');
        $macchineFolder = array_slice(scandir($macchinePath), 2, count(scandir($macchinePath)));
        sort($macchineFolder);
        if(count($macchineFolder)>0){
            $lastId = $macchineFolder[count($macchineFolder) - 1];
        }else{
            $lastId = 0;
        }
        $macchineId = intval($lastId) + 1;
        $newMacchinePath = $macchinePath.$macchineId;
        /*
         * Create macchine folder
         */
        mkdir($newMacchinePath, 0777);
        $newMacchinePathImages = $newMacchinePath.'/images';
        mkdir($newMacchinePathImages, 0777);
        $imagesList = [];
        $imgCount = count($images);
        foreach($images as $img){
            
            $imgCount++;
            $extension = $img->getClientOriginalExtension();
            $fileName = $imgCount.'.'.$extension;
            $img->move($newMacchinePathImages, $fileName);
            $imagesList[] = $newMacchinePathImages.'/'.$fileName;
        }
        $imgCount = 0;
        $newMacchineInfo = [
            'id' => $macchineId,
            'name' => $input['name'],
            'year' => $input['year'],
            'type' => $input['type'],
            'currency' => $input['currency'],
            'price' => $input['price'],
            'description' => $input['description'],
            'images' => $imagesList
        ];
        $jsonMacchine = json_encode($newMacchineInfo);
        $macchineDetailsFile = 'macchine.json';
        File::put($newMacchinePath.'/'.$macchineDetailsFile, $jsonMacchine);
        
        return Redirect::to("admin\macchine");
    }
    
    public function removeMacchine($id) {
        $macchinePath = public_path('resources/macchine/');
        $newMacchinePath = $macchinePath.$id;
        if(File::deleteDirectory($newMacchinePath)){
            return Redirect::to("admin\macchine");
        }else{
            dd('cant delete folder! in '.__CLASS__);
        }
    }
    
    
}
