<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class BaseController extends Controller
{
    
    /*
     * User auth flag
     * @type boolean
     */
    protected $isAuth;
    
    public $viewBag;


    public function getMacchine(){
        $macchines = [];
        $macchinePath = public_path('resources/macchine/');
        $macchineFolders = array_slice(scandir($macchinePath), 2, count(scandir($macchinePath)));
        
        if(count($macchineFolders) > 0){
            foreach($macchineFolders as $folder){
                $json = File::get($macchinePath.$folder."/macchine.json");
                $macchines[] = json_decode($json);
            }
        }
        return $macchines;
    }

    public function __construct() {
        $this->viewBag = [];
        $this->isAuth = Session::has('isAuth');
        $this->viewBag['macchine'] = $this->getMacchine();
    }
    
    
}
