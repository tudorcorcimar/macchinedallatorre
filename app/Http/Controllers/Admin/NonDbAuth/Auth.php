<?php

namespace App\Http\Controllers\Admin\NonDbAuth;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Entities\NonDbUser;
use Illuminate\Support\Facades\Session;

class Auth extends BaseController
{
    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function showSignIn(){
        if($this->isAuth){
            return Redirect::to('admin/macchine');
        }
        return View::make("admin.auth.signin", $this->viewBag);
    }
    
    public function SignIn() {
        $this->viewBag['input']['login'] = Input::get('login');
        $isValid = $this->isValidCredentials();
        if($isValid){
            Session::put('isAuth', $isValid);
            return Redirect::to('admin/macchine');
        }else{
            $this->viewBag['authError'] = 'Login or password are inccorect!';
            return View::make("admin.auth.signin", $this->viewBag);
        }
    }
    
    protected function isValidCredentials() {
        $input = Input::all();
        return $input['login'] === NonDbUser::LOGIN && $input['password'] === NonDbUser::PASSWORD;
    }
    
    
}
