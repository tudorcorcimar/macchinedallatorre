<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Services\ValidatorConfig;
use App\Models\Services\Captcha;
use Illuminate\Support\Facades\Mail;

class Contacts extends BaseController
{
    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function showContacts(){
        $this->viewbag['moduleName'] = 'contacts'; 
        return View::make("frontend.home.contacts", $this->viewbag);
    }
    
    public function send(){
        $this->viewbag['moduleName'] = 'contacts';
        $this->viewbag['form'] = Input::all();
        $contactsRules = [
            'name'         => 'required',
            'email'        => 'required|email',
            'subject'      => 'required|max:255',
            'message'      => 'required|min:25',
            'captcha'      => 'required|captcha'
        ];
        $validator = ValidatorConfig::validate($contactsRules);
        
        if($validator->fails()){
            $this->viewbag['validatorErrors'] = $validator->errors();
            return View::make("frontend.home.contacts", $this->viewbag);
        }else{
            Mail::send('frontend.emails.contacts', $this->viewbag, function($message){
                $message->from($this->viewbag['form']['email'], $this->viewbag['form']['email']);
                $message->to([$this->viewbag['app']['email'], 'nadejdacorcimar@gmail.com'], 'Internal: Contacts from - '.$this->viewbag['form']['email'] )
                        ->subject('Internal: Contacts from - '.$this->viewbag['form']['email']);
            });
            Captcha::refrashCaptcha();
            return redirect('/contacts');
        }
        
    }
    
}
