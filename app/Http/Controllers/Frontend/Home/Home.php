<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;


class Home extends BaseController
{
    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function showHome(){
        $this->viewbag['moduleName'] = 'home'; 
        return View::make("frontend.home.home", $this->viewbag);
    }
    
    public function showAboutUs(){
        $this->viewbag['moduleName'] = 'aboutus'; 
        return View::make("frontend.home.aboutus", $this->viewbag);
    }
    
}
