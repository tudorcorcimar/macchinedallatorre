<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class BaseController extends Controller
{
    /**
     * @var array Variable to store all view data.
     */
    protected $viewbag = array();
    
    /**
     * @const string Company meta Seo tags
     * 
     * COMPANY_META_TITLE ---------- <title>title of page</title> important for SEO
     * COMPANY_META_DESCRIPTION ---- <meta name="description" content="some page description"> max 160 symbols 
     * COMPANY_META_DEFAULT_ROBOTS - <meta name="robots" content="index,nofollow">
     *                               content = all | none | directives
     *                               all = «ALL»
     *                               none = «NONE»
     *                               directives = directive [«,» directives]
     *                               directive = index | follow
     *                               index = «INDEX» | «NOINDEX»
     *                               follow = «FOLLOW» | «NOFOLLOW»
     * COMPANY_META_CONTENT_TYPE --- <meta http-equiv="Content-Type" content="Type=text/html; charset=utf-8">
     *                               short version of current metateg <meta charset="utf-8" />
     *                               this is not required for SEO development and it's better to use short version
     *                               it need to set befor <title>
     * COMPANY_META_KEYWORDS ------- <meta name="keywords" content="keywords" /> this tag will not influence to 
     *                               ranking of web resource most seach sistems will ignore it
     * COMPANY_META_LANGUAGE ------- <meta http-equiv="content-language" content="fr" />
     *                               today most used is short version <html lang="en">
     *                               and it can be used for other tags as <p lang="es">Me gusta..
     * COMPANY_META_TRANSLATION ---- <meta name="google" content="notranslate" /> just if dont wont to translate our page
     * COMPANY_META_REFRASH -------- <meta http-equiv="refresh" content="30"> if we wot to refrash browser page after 30 seconds
     *                               <meta http-equiv="refresh" content="30;URL='google.com'"> refrash with redirect
     * 
     * note: most important tags is DESCRIPTION, ROBOTS, TITLE, H1, H2, H3
     * 
     */
    const COMPANY_META_TITLE          = 'MacchineDallaTorre';
    const COMPANY_META_DESCRIPTION    = 'MacchineDallaTorre: Vendita di macchine utensili nuove, usate e revisionate.';
    const COMPANY_META_DEFAULT_ROBOTS = 'index, follow';
    const COMPANY_META_CONTENT_TYPE   = 'Type=text/html; charset=utf-8';
    const COMPANY_META_KEYWORDS       = 'Macchine,MacchineDallaTorre,utensili,usate';
    const COMPANY_META_LANGUAGE       = 'it';
    const COMPANY_META_TRANSLATION    = 'notranslate';
    const COMPANY_META_REFRASH_TIME   = '3000'; // seconds
    const COMPANY_META_REFRASH_URL    = ''; // url
    
    /**
     * @const string Company general info
     */
    const COMPANY_NAME    = 'MacchineDallaTorre';
    const COMPANY_URL     = 'www.MacchineDallaTorre.com';
    const COMPANY_EMAIL   = 'andreadallatorre55@gmail.com';
    const COMPANY_PHONE   = '+39 3358296772';
    const COMPANY_SKYPE   = 'vivacars';
    const COMPANY_ADDRESS = 'Sede Legale e Amministrativa <br/> Via Beato Odorico, 13 <br/> 33170 PORDENONE <br/> C.F.DLLNDR70C21G888F <br/> P.Iva 01192360939';
    
    /**
     * @const string Social networks url
     */
    const COMPANY_SOCIAL_FACEBOOK_URL = 'https://www.facebook.com/pages/Macchine-Dallatorre/216855461836435?id=216855461836435&sk=info';
    const COMPANY_SOCIAL_TWITTER_URL  = '#';
    const COMPANY_SOCIAL_LINKEDIN_URL = '#';
    
    /**
     * @const string Macchine
     */
    const MACCHINE_PATH = 'resources/macchine';
    const MACCHINE_IMAGES = 'img';
    const MACCHINE_INFO = 'info.json';
    
    /**
     * Get general view data
     */
    private function getViewbag() {
        
        // Viewbag collector
        $bag                                   = [];
        
        // Application array`
        $bag['app']                            = [];
        
        // Collect meta info
        $bag['app']['meta']                    = [];
        $bag['app']['meta']['title']           = self::COMPANY_META_TITLE;
        $bag['app']['meta']['description']     = self::COMPANY_META_DESCRIPTION;
        $bag['app']['meta']['robots']          = self::COMPANY_META_DEFAULT_ROBOTS;
        $bag['app']['meta']['contenttype']     = self::COMPANY_META_CONTENT_TYPE;
        $bag['app']['meta']['keywords']        = self::COMPANY_META_KEYWORDS;
        $bag['app']['meta']['language']        = self::COMPANY_META_LANGUAGE;
        $bag['app']['meta']['translation']     = self::COMPANY_META_TRANSLATION;
        // Meta refrash
        $bag['app']['meta']['refrash']         = [];
        $bag['app']['meta']['refrash']['time'] = self::COMPANY_META_REFRASH_TIME;
        $bag['app']['meta']['refrash']['url']  = self::COMPANY_META_REFRASH_URL;
        
        // Collect company base info
        $bag['app']['name']                    = self::COMPANY_NAME;
        $bag['app']['url']                     = self::COMPANY_URL;
        $bag['app']['email']                   = self::COMPANY_EMAIL;
        $bag['app']['phone']                   = self::COMPANY_PHONE;
        $bag['app']['skype']                   = self::COMPANY_SKYPE;
        $bag['app']['address']                 = self::COMPANY_ADDRESS;
        
        // Collect social info
        $bag['app']['social']['facebook']['url']      = self::COMPANY_SOCIAL_FACEBOOK_URL;
        $bag['app']['social']['twitter']['url']       = self::COMPANY_SOCIAL_TWITTER_URL;
        $bag['app']['social']['linkedin']['url']      = self::COMPANY_SOCIAL_LINKEDIN_URL;
        
        return $bag;
    }


    public function getMacchine(){
        $macchines = [];
        $macchinePath = public_path('resources/macchine/');
        $macchineFolders = array_slice(scandir($macchinePath), 2, count(scandir($macchinePath)));
        if(count($macchineFolders) > 0){
            foreach($macchineFolders as $folder){
                $json = File::get($macchinePath.$folder."/macchine.json");
                $macchines[] = json_decode($json);
            }
        }
        return $macchines;
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->viewbag = self::getViewbag();
        $this->viewbag['macchine'] = $this->getMacchine();
    }
    
}
