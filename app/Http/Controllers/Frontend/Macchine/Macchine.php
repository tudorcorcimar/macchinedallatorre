<?php

namespace App\Http\Controllers\Frontend\Macchine;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;


class Macchine extends BaseController
{
    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function showMacchine(){ 
        $this->viewbag['moduleName'] = 'macchine';
        return View::make("frontend.macchine.list", $this->viewbag);
    }
    
    
    
}
