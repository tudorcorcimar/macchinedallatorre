<?php

namespace App\Models\Services;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;

class Captcha
{
    public static function refrashCaptcha(){
        return self::getCaptcha();
    }

    public static function getCaptcha($size = 4){
        
        $string = '';
        // create image
        $imageWidth  = 159;
        $imageHeight = 48;
        $image     = imagecreatetruecolor($imageWidth, $imageHeight);
        // get white color for background of image
        $white     = imagecolorallocate($image, 45, 45, 45);
        // fill background of image with white color
        imagefilledrectangle($image, 0,0,200,100, $white);
        // set font size
        $fontSize = 12;
        // number of symbols
        $symbolsCount = $size;
        // get width fo symbol block
        $symbolBlock = $imageWidth/$symbolsCount;
        
        $positionX = round(($symbolBlock)-($fontSize/2)-4);
        $positionY = 28;
        
        for($i = 0; $i < $symbolsCount; $i++){
            $randSymbol = self::generateRandomSymbol();
            $randColor = imagecolorallocate($image, rand( 1, 255 ), rand( 1, 255 ), rand( 1, 255 ));
            putenv('GDFONTPATH=' . realpath('.'));
            $font = realpath('.').'/fonts/captcha/'.self::getRandomFont();
            imagettftext ($image, $fontSize, self::generateRandomAngle(), $positionX*($i+1), $positionY, $randColor, $font, $randSymbol);
            $string = $string.$randSymbol;
        }
        
        Session::put('captcha', $string);

        ob_start(); // start a new output buffer
        imagepng($image, null);
        $imagePng = ob_get_contents();
        $size = ob_get_length();
        ob_end_clean(); // stop this output buffer
        imagedestroy($image);
        
        return Response::make($imagePng, 200, array('Content-Type' => 'image/jpeg', 'Content-Length' => $size));
    }
    
    public static function is($formCaptcha){
        $realCaptcha = Session::get('captcha');
        if($formCaptcha == $realCaptcha){
            return true;
        }
        return false;
    }


    private static function generateRandomSymbol() {
        $characters = '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = $characters[rand(0, $charactersLength - 1)];
        return $randomString;
    }
    
    private static function getRandomFont(){
        $fontsList = [
            'ArchitectsDaughter',
            'Bangers',
            'Chewy',
            'CoveredByYourGrace',
            'Dosis-Medium',
            'Exo2-SemiBold',
            'GloriaHallelujah',
            'Muli-Regular',
            'Orbitron-Medium',
            'RockSalt',
            'SigmarOne',
            'Slabo27px-Regular',
            'YanoneKaffeesatz-Regular',
            'AnonymousPro-Italic',
            'BadScript-Regular',
            'CevicheOne-Regular',
            'CrimsonText-BoldItalic',
            'PlayfairDisplaySC-Regular'
        ];
        $extension = '.ttf';
        //$extension = '';
        $randomFont = rand( 1, 17);
        return $fontsList[$randomFont].$extension;
    }
    
    private static function generateRandomAngle(){
        return rand( 340, 380);
    }
    
}
