<?php

use Phinx\Migration\AbstractMigration;

class CreateMacchinesTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE macchine ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " title VARCHAR(255) DEFAULT '', "
                       . " images TEXT, "
                       . " description TEXT NOT NULL DEFAULT '', "
                       . " price INT(11), "
                       . " currency VARCHAR(5), "
                       . " status INT(1), "
                       . " year VARCHAR(50), "
                       . " posted_date DATE, "
                       . " PRIMARY KEY (id) )");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE macchine;");
    }
}
