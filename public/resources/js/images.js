var popupImg = {
    setImage : function(modalIndex, imageIndex){
        var modalClass = '.modal-img-' + modalIndex;
        var modalImages = $('.img-body img', $(modalClass));
        modalImages.removeClass('active');
        var index = imageIndex - 1;
        var image = modalImages[index];
        $(image).addClass('active');
    },
    next : function(elem){
        var images = $('.image > img', elem.closest('.img-body'));
        var imageNr = null;
        $.each(images, function(index ){
            if($(images[index]).hasClass('active')){
                
                if(index == images.length - 1){
                    imageNr = 0;
                }else{
                    imageNr = index + 1;
                }
                $(images[index]).removeClass('active');
            }
        });
        $(images[imageNr]).addClass('active');
    },
    previous : function(elem){
        var images = $('.image > img', elem.closest('.img-body'));
        var imageNr = null;
        $.each(images, function(index ){
            if($(images[index]).hasClass('active')){
                if(index == 0){
                    imageNr = images.length - 1;
                }else{
                    imageNr = index - 1;
                }
                $(images[index]).removeClass('active');
            }
        });
        $(images[imageNr]).addClass('active');
    }
};